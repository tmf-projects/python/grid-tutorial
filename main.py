﻿import tkinter as tk

# Rust Language
#
# fn main() {
#   let mut a = 10;
#   let b = &mut a;
#   *b = 12;
#   println!("{}", b)
# }

root = tk.Tk()

x1_l = tk.Label(text="x1").grid(row=0, column=0, sticky=tk.W, pady=5, padx=5)
x1_entry = tk.Entry().grid(row=0, column=1, columnspan=1, sticky=tk.W+tk.E, padx=5)

x2_l = tk.Label(text="x2").grid(row=1, column=0, sticky=tk.W, pady=5, padx=5)
x2_entry = tk.Entry().grid(row=1, column=1, columnspan=1, sticky=tk.W+tk.E, padx=5)

y1_l = tk.Label(text="y1").grid(row=0, column=3, sticky=tk.W, pady=5, padx=5)
y1_entry = tk.Entry().grid(row=0, column=4, columnspan=1, sticky=tk.W+tk.E, padx=5)

y2_l = tk.Label(text="y2").grid(row=1, column=3, sticky=tk.W, pady=5, padx=5)
y2_entry = tk.Entry().grid(row=1, column=4, columnspan=1, sticky=tk.W+tk.E, padx=5)

rect = tk.Checkbutton(text="Прямоугольник").grid(row=2, column=2, columnspan=1, sticky=tk.W+tk.E, padx=10)
oval = tk.Checkbutton(text="Овал").grid(row=3, column=2, columnspan=1, sticky=tk.W+tk.E, padx=10)

btn = tk.Button(text="Нарисовать").grid(row=4, column=2, columnspan=1, sticky=tk.W+tk.E, pady=10, padx=10)

root.mainloop()



